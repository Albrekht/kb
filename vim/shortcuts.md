##### NerdTree shortcuts
 
`,nn` # Toggle on/off  
`Ctrl-W + Arrow(right/left)` - Toggle between files and Tree  
`Shift+t` - Open file in new tab

##### Vim tab
`gt` - go to next tab  
`gT` - go to previous tab  
`{i}gt` - go to tab in position i  