# Description

KB about Ansible

# Table of contents

* [Install](install.md)
* [Update](update.md)
* [Ansible CLI](cli.md)
* [Ansible-Playbook](playbook.md)
* [My Ansible Roles](role.md)
* [F.A.Q](faq.md)
* [Useful links](links.md)