# Description

This is my personal Knowladge Base

# Disclaimer

#### This REPO in under heavy construction!

# Table of contents

* [Linux (Mac)](linux)
    * Basics
    * [Users](linux/users.md)
    * Packages
    * Network
    * Bash
    * Sed/Awk
    * MacOS
* [Vim](vim)
* [Web](web)
    * [Nginx](web/nginx)
    * [Let's Encrypt](web/le) 
* [Docker](docker)
* [Ansible](ansible)
* [Vagrant](vagrant)
* [Backups](backups)
    * [Borg](backups/borg)
    * [Rsync](backups/rsync)
    * [Bacula](backups/bacula)
* [Logs](logs)
* [Monitoring](monitoring)
* [Mail](mail)

# Test Line