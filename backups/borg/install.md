# Description

Docker installation on various platforms

# Table of contents

* Install on Debian/Ubuntu
    * Manual
    * Script
    * Ansible
* Install on RHEL/CentOS
    * Manual
    * Script
    * Ansible

## Install on Debian/Ubuntu

## Install on RHEL/CentOS

```bash
# From EpelREPO
sudo yum install epel-release -y
sudo yum install borgbackup

# From Binary
sudo wget https://github.com/borgbackup/borg/releases/download/1.1.6/borg-linux64 -O /usr/local/bin/borg
sudo chmod +x /usr/local/bin/borg
```