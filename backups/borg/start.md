# Description

Make your first backup with BORG

### Preparation

On client:
```bash
ssh-keygen
```

On backup server:
```bash
sudo useradd -m borg
mkdir ~borg/.ssh
echo 'command="/usr/local/bin/borg serve" ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDNdaDfqUUf/XmSVWfF7PfjGlbKW00MJ63zal/E/mxm+vJIJRBw7GZofe1PeTpKcEUTiBBEsW9XUmTctnWE6p21gU/JNU0jITLx+vg4IlVP62cac71tkx1VJFMYQN6EulT0alYxagNwEs7s5cBlykeKk/QmteOOclzx684t9d6BhMvFE9w9r+c76aVBIdbEyrkloiYd+vzt79nRkFE4CoxkpvptMgrAgbx563fRmNSPH8H5dEad44/Xb5uARiYhdlIl45QuNSpAdcOadp46ftDeQCGLc4CgjMxessam+9ujYcUCjhFDNOoEa4YxVhXF9Tcv8Ttxolece6y+IQM7fbDR' > ~borg/.ssh/authorized_keys
chown -R borg:borg ~borg/.ssh
```

From client test connection and make repo for backups:
```bash
ssh borg@backup 'borg -V' # просто для проверки соединения
borg init -e none borg@backup:CLIENT-etc
```

### Make your first backup

Make your first backup and check it:
```bash
borg create --stats --list borg@backup:CLIENT-etc::"etc-{now:%Y-%m-%d_%H:%M:%S}" /etc
borg list borg@backup:CLIENT-etc
borg list borg@backup:CLIENT-etc::etc-2018-12-11_09:30:44
```

For extracting backup use `extract` command or `mount`:
```bash
borg extract borg@backup:CLIENT-etc::etc-2018-12-11_09:30:44 etc/hostname
borg mount borg@backup:CLIENT-etc::etc-2018-12-11_09:30:44 /mnt/
```