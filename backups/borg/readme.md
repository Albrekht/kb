# Description

Typical tasks and thair automation

# Table of contents

* [Install](install.md)
* [Update](update.md)
* [Quick start](start.md)
* [Borg CLI](cli.md)
* [F.A.Q](faq.md)
* [Useful links](links.md)