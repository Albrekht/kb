# Description

Typical tasks and thair automation

# Table of contents

* [Install](install.md)
* [Update](update.md)
* [Docker CLI](cli.md)
* [Dockerfile](dockerfile.md)
* [F.A.Q](faq.md)
* [Useful links](links.md)
