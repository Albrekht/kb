# DOCKER

## Basic

```bash
docker ps -a
docker logs
docker top
docker stats --all
docker inspect 
docker diff
docker system df
docker exec -it cnt_name /bin/sh
docker rm $(docker )
```

## Images

```bash
docker images
docker import # Create image from tarballt
docker build
docker commit # Create image from container
docker rmi
```

## Registry & Repo

```bash
docker login
docker logout
docker search
docker pull # Pull image from registry
docker push # Push imate to registry
```

## Prune

```bash
docker system prune
docker volume prune
docker network prune
docker container prune
docker image prune
```

## Hints & Tips

```bash
docker kill $(docker ps -q) # Kill all running containers
docker rm -f $(docker ps -qa) # Delete all containers
docker rm -v $(docker ps -a -q -f status=exited) # Delete only stopped containers
docker rmi $(docker images -q) # Delete all images
```
