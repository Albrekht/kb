# Description

Useful links about Docker

# Links

### Tutorials
https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker - NGINX Reverse Proxy with Docker  

### Read
https://habr.com/post/334004/ - inside Docker network (rus)  
https://medium.freecodecamp.org/docker-entrypoint-cmd-dockerfile-best-practices-abc591c30e21 - Entrypoint and CMD  

### Examples
https://github.com/stefanprodan/swarmprom - Docker Swarm  

