# Description

Solving users related tasks

# Tasks

1. Add user on Debian 7/8

```bash
useradd username -m -d /home/username -s /bin/bash -p 12345
chown -R username.username /home/username/
passwd username
```

2. Add user ssh key

```bash
mkdir /home/username/.ssh
echo "PUBLIC KEY" >> /home/username/.ssh/authorized_keys
chown -R username:username /home/username/.ssh
chmod 600 /home/username/.ssh/authorized_keys
```

3. Add User to sudo

### Debian 
`sudo usermod -a -G sudo username`

# Scripts

