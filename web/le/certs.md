# Description

Working with Let's Encrypt certificates

# Table of contents



## Obtain new certificate

1. First ensure that you have properly configured `acme.conf` or simply conf for acme:
```nginx
location = /.well-known/acme-challenge/ {
      return 404;
  }

  location ^~ /.well-known/acme-challenge/ {
    default_type "text/plain";
    root /var/www/acme;
  }
```
> Be sure to use only valid domains and DNS that lead to this server

2. Run certbot to obtain new certificate. First `dry-run` to test:

```bash
certbot certonly -w /var/www/acme --dry-run -d your.hostname.com
certbot certonly -w /var/www/acme -d your.hostname.com
```
> Note that you can pass multiply -d to certbot.

3. That all for cert. Next you must configure https redirect on your website. You can do it by this [manual](https://gitlab.com/Albrekht/kb/blob/master/web/nginx/configs/https.md)