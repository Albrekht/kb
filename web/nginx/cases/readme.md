### Manual update on Debian 8 Jessie https://youtrack.gdemoi.ru/issue/SYS-808

```bash
# Add repo key
wget -O - http://nginx.org/keys/nginx_signing.key | sudo apt-key add -

# Make a backup copy of your current sources.list file
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

# Now copy the following repositories to the end of ` /etc/apt/sources.list`
echo "deb http://nginx.org/packages/mainline/debian/ jessie nginx" | tee -a /etc/apt/sources.list
echo "deb-src http://nginx.org/packages/mainline/debian/ jessie nginx" | tee -a /etc/apt/sources.list

# Remove previous version
sudo apt remove nginx nginx-common nginx-full

# Update to get the new repository
sudo apt-get update
sudo apt-get install nginx

# Install image-filter module
apt-get install nginx-module-image-filter

# Remove and add image-filter module file
rm /etc/nginx/modules-enabled/50-mod-http-image-filter.conf
echo "load_module modules/ngx_http_image_filter_module.so;" > /etc/nginx/modules-enabled/50-mod-http-image-filter.conf

# Replace IP bindings
find /etc/nginx/sites-available/ -type f | xargs sed -i  's/172.16.5.55:443/172.16.5.120:443/g'
find /etc/nginx/sites-available/ -type f | xargs sed -i  's/172.16.5.65:443/172.16.5.121:443/g'
find /etc/nginx/sites-available/ -type f | xargs sed -i  's/172.16.5.55:80/172.16.5.120:80/g'
find /etc/nginx/sites-available/ -type f | xargs sed -i  's/172.16.5.65:80/172.16.5.121:80/g'
```