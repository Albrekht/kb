# Description

NGINX update on various platforms

# Table of contents

* Update on Debian/Ubuntu
    * Manual
    * Script
    * Ansible
* Install on RHEL/CentOS
    * Manual
    * Script
    * Ansible

## Update on Debian/Ubuntu