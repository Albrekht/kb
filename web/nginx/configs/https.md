1. vim /etc/nginx/sites-available/my.gps-experts.com
```
server {
    listen 172.16.5.55:80;
    server_name my.gps-experts.com;
    include acme.conf;
    location / {
      return 301 https://$host$request_uri;
    }
}
```

```
ln -s /etc/nginx/sites-available/my.gps-experts.com /etc/nginx/sites-enabled/my.gps-experts.com
nginx -t
nginx -s reload
```

2. vim /etc/nginx/sites-available/my.gps-experts.com_ssl
```
server {
    listen  172.16.5.55:443 ssl;

    ssl_certificate /etc/letsencrypt/live/my.gps-experts.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/my.gps-experts.com/privkey.pem;

    ssl on;
    server_name my.gps-experts.com;
    server_name_in_redirect off;
    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout 10m;
    ssl_prefer_server_ciphers on;
    ssl_ciphers kEECDH+AES128:kEECDH:kEDH:-3DES:kRSA+AES128:kEDH+3DES: \ 
    DES-CBC3-SHA:!RC4:!aNULL:!eNULL:!MD5:!EXPORT:!LOW:!SEED:!CAMELLIA:!IDEA:!PSK:!SRP:!SSLv2;
    ssl_protocols TLSv1.2;
    ssl_dhparam /etc/nginx/ssl/dhparams.pem;
    ssl_stapling on;

    add_header Strict-Transport-Security "max-age=31536000";
    add_header Content-Security-Policy "block-all-mixed-content";

    include include/saas.navixy.com;
}
```


```
ln -s /etc/nginx/sites-available/my.gps-experts.com_ssl /etc/nginx/sites-enabled/my.gps-experts.com_ssl
nginx -t
nginx -s reload
```