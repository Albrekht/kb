# Description

Typical tasks and thair automation

# Table of contents

* [Install](install.md)
* [Update](update.md)
* [Vagrant CLI](cli.md)
* [Vagrantfile](vagrantfile.md)
* [Tips and triks](tips.md)
* [F.A.Q](faq.md)
* [Useful links](links.md)